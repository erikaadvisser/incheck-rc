package org.n1.va.incheck.server.rest.register.repo;

import org.joda.time.LocalDate;
import org.n1.va.incheck.server.rest.register.model.ValeaBankBoeking;
import org.n1.va.incheck.server.rest.register.model.ValeaBoekingMapping;
import org.n1.va.incheck.server.rest.register.model.ValeaDeelnemer;
import org.n1.va.incheck.server.rest.util.AbstractRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class PaymentHistoryRepo extends AbstractRepo {

    @Autowired
    public PaymentHistoryRepo(DataSource dataSource) {
        super(dataSource);
    }

    public List<ValeaBankBoeking> getHistory(ValeaDeelnemer deelnemer) {
        LocalDate lastYear = LocalDate.now().minusYears(1).minusMonths(6);
        Date lastYearAsDate = lastYear.toDate();

        Map<String, Object> params = new HashMap<>();
        params.put("cutoffTime", lastYearAsDate);
        params.put("deelnemerId", deelnemer.id);

        String sql = "SELECT distinct kasboeking.*" +
                " FROM kasboeking, boeking_event, events" +
                " WHERE boeking_event.boeking_id = kasboeking.id" +
                " AND boeking_event.event_id = events.id" +
                " AND events.deelnemer_id = :deelnemerId" +
                " AND kasboeking.datum > :cutoffTime" +
                " ORDER BY kasboeking.datum DESC";

        List<ValeaBankBoeking> boekingen = namedTemplate.query(sql, params,
                ParameterizedBeanPropertyRowMapper.newInstance(ValeaBankBoeking.class));

        return boekingen;
    }

    public List<ValeaBoekingMapping> getMappings(ValeaBankBoeking boeking) {
        Map<String, Object> params = new HashMap<>();
        params.put("boekingId", boeking.id);

        String sql = "SELECT * " +
                " FROM boeking_event" +
                " WHERE boeking_id = :boekingId";

        List<ValeaBoekingMapping> mappings = namedTemplate.query(sql, params,
                ParameterizedBeanPropertyRowMapper.newInstance(ValeaBoekingMapping.class));

        return mappings;
    }
}
