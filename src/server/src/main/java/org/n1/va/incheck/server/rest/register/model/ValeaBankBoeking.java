package org.n1.va.incheck.server.rest.register.model;

import java.util.Date;

/**
 * Class representing a boeking
 */
public class ValeaBankBoeking {
    public long id;
    public Long bank_entry_id;
    public double bedrag;
    public String naam_tegenrekening;
    public Date datum;
    public String kassa;
    public int bladnummer;
    public String omschrijving1;
    public String omschrijving2;
    public String omschrijving3;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getBank_entry_id() {
        return bank_entry_id;
    }

    public void setBank_entry_id(Long bank_entry_id) {
        this.bank_entry_id = bank_entry_id;
    }

    public double getBedrag() {
        return bedrag;
    }

    public void setBedrag(double bedrag) {
        this.bedrag = bedrag;
    }

    public String getNaam_tegenrekening() {
        return naam_tegenrekening;
    }

    public void setNaam_tegenrekening(String naam_tegenrekening) {
        this.naam_tegenrekening = naam_tegenrekening;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public String getKassa() {
        return kassa;
    }

    public void setKassa(String kassa) {
        this.kassa = kassa;
    }

    public int getBladnummer() {
        return bladnummer;
    }

    public void setBladnummer(int bladnummer) {
        this.bladnummer = bladnummer;
    }

    public String getOmschrijving1() {
        return omschrijving1;
    }

    public void setOmschrijving1(String omschrijving1) {
        this.omschrijving1 = omschrijving1;
    }

    public String getOmschrijving2() {
        return omschrijving2;
    }

    public void setOmschrijving2(String omschrijving2) {
        this.omschrijving2 = omschrijving2;
    }

    public String getOmschrijving3() {
        return omschrijving3;
    }

    public void setOmschrijving3(String omschrijving3) {
        this.omschrijving3 = omschrijving3;
    }
}
