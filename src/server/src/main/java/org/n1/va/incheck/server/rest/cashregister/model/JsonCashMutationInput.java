package org.n1.va.incheck.server.rest.cashregister.model;

/**
 * An incoming mutation
 */
public class JsonCashMutationInput {

    public String user;
    public String reason;
    public double amount;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
