package org.n1.va.incheck.server.rest.cashregister.model;

import java.util.List;

/**
 * container for cash report
 */
public class JsonCashMutationReport {

    public double total;
    public List<JsonCashMutation> mutations;

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public List<JsonCashMutation> getMutations() {
        return mutations;
    }

    public void setMutations(List<JsonCashMutation> mutations) {
        this.mutations = mutations;
    }
}
