package org.n1.va.incheck.server.rest.util;

import org.n1.va.incheck.server.rest.register.model.ValeaDeelnemer;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import javax.sql.DataSource;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * This is a wrapper around a Jdbc templat that logs the queries to file as a sort of redo log.
 */
public class RedoLogTemplate {

    private JdbcTemplate template;
    private RedoLogger redoLogger;

    protected RedoLogTemplate(JdbcTemplate template, RedoLogger redoLogger) {
        super();
        this.template = template;
        this.redoLogger = redoLogger;
    }

    public int update(java.lang.String sql, java.lang.Object... args)  {
        redoLogger.redoLog(sql, args);
        return template.update(sql, args);
    }

    public <T> List<T> query(String sql, ParameterizedRowMapper<T> mapper, Object... args) {
        return template.query(sql, mapper, args);
    }

    public <T> T queryForObject(String sql, ParameterizedRowMapper<T> mapper, Object... args) {
        return template.queryForObject(sql, mapper, args);
    }

    public Long queryForLong(String sql) {
        return template.queryForLong(sql);
    }

    public <T> T queryForObject(String sql, Class<T> clazz, Object... args) {
        return template.queryForObject(sql, clazz, args);
    }
}
