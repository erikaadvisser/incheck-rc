package org.n1.va.incheck.server.rest.report;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * A single row in the mutations report.
 */
public class MutationRow {

    private static SimpleDateFormat dagFormat = new SimpleDateFormat("YYYY-MM-dd");
    private static SimpleDateFormat tijdFormat = new SimpleDateFormat("HH:mm:ss");

    public String kassa;
    public String bedrag;
    public String omschrijving;
    public String gebruiker;
    public String tijd;
    public String dag;

    public String getKassa() {
        return kassa;
    }

    public void setKassa(String kassa) {
        this.kassa = kassa;
    }

    public String getBedrag() {
        return bedrag;
    }

    public void setBedrag(String bedrag) {
        this.bedrag = bedrag;
    }

    public String getOmschrijving() {
        return omschrijving;
    }

    public void setOmschrijving(String omschrijving) {
        this.omschrijving = omschrijving;
    }

    public String getGebruiker() {
        return gebruiker;
    }

    public void setGebruiker(String gebruiker) {
        this.gebruiker = gebruiker;
    }

    public String getTijd() {
        return tijd;
    }

    public void setTijdstip(Date tijd) {
        this.tijd = tijdFormat.format(tijd);
        this.dag = dagFormat.format(tijd);
    }

    public String getDag() {
        return dag;
    }
}
