package org.n1.va.incheck.server.rest.extract;

import org.n1.va.incheck.server.rest.util.AbstractRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ExtractRepo extends AbstractRepo {

    @Autowired
    public ExtractRepo(DataSource dataSource) {
        super(dataSource);
    }

    public List<ExtractRow> getExtract() {
        Map<String, Object> params = new HashMap<String, Object>();
        List<ExtractRow> rows= namedTemplate.query(
                "SELECT plin, concat(trim(concat_ws(' ', voornaam, tussenvoegsels)), ' ', achternaam) name FROM deelnemers",
                params,
                ParameterizedBeanPropertyRowMapper.newInstance(ExtractRow.class)
        );
        return rows;
    }
}
