package org.n1.va.incheck.server.rest.register.jsonmodel;


import org.n1.va.incheck.server.rest.register.model.ValeaDeelnemer;

/**
 * Info used to register a person
 */
public class RegisterInfo {


    public String plin;
    public String name;
    public Long dateOfBirthMillis;
    public boolean member;
    public String email;
    public String roleCode;
    public String roleDescription;
    public boolean infoComplete = false;
    public RegisterPaymentInfo paid = new RegisterPaymentInfo();
    public RegisterPaymentBooleans paidInFull = new RegisterPaymentBooleans();
    public boolean checkedIn;

    public Exception error;

    public String getPlin() {
        return plin;
    }

    public RegisterInfo() {
    }

    public RegisterInfo(ValeaDeelnemer deelnemer) {
        this.plin = deelnemer.plin;
        this.name = deelnemer.name;
        if (deelnemer.dateOfBirth != null) {
            this.dateOfBirthMillis = deelnemer.dateOfBirth.getTime();
        }
        else {
            this.dateOfBirthMillis = null;
        }
        this.email = deelnemer.email;

        if (this.plin != null && this.dateOfBirthMillis != null && this.name != null && this.name.trim().length() > 0) {
            this.infoComplete = true;
        }
        if (deelnemer.lid_start != null && deelnemer.lid_eind == null) {
            member = true;
        }
    }

    public void setPlin(String plin) {
        this.plin = plin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getDateOfBirthMillis() {
        return dateOfBirthMillis;
    }

    public void setDateOfBirthMillis(Long dateOfBirthMillis) {
        this.dateOfBirthMillis = dateOfBirthMillis;
    }

    public boolean isMember() {
        return member;
    }

    public void setMember(boolean member) {
        this.member = member;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public RegisterPaymentInfo getPaid() {
        return paid;
    }

    public void setPaid(RegisterPaymentInfo paid) {
        this.paid = paid;
    }

    public RegisterPaymentBooleans getPaidInFull() {
        return paidInFull;
    }

    public void setPaidInFull(RegisterPaymentBooleans paidInFull) {
        this.paidInFull = paidInFull;
    }

    public boolean isCheckedIn() {
        return checkedIn;
    }

    public void setCheckedIn(boolean checkedIn) {
        this.checkedIn = checkedIn;
    }

    public boolean isInfoComplete() {
        return infoComplete;
    }

    public void setInfoComplete(boolean infoComplete) {
        this.infoComplete = infoComplete;
    }

    public Exception getError() {
        return error;
    }

    public void setError(Exception error) {
        this.error = error;
    }

    public String getRoleDescription() {
        return roleDescription;
    }

    public void setRoleDescription(String roleDescription) {
        this.roleDescription = roleDescription;
    }
}


