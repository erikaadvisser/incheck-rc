package org.n1.va.incheck.server.rest.register.jsonmodel;

import java.util.List;

/**
 * Represents a single received boeking
 */
public class PaymentHistoryBoeking {

    public double amount;
    public long date;
    public String payer;
    public String description;
    public List<PaymentHistoryMapping> mappings;

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getPayer() {
        return payer;
    }

    public void setPayer(String payer) {
        this.payer = payer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<PaymentHistoryMapping> getMappings() {
        return mappings;
    }

    public void setMappings(List<PaymentHistoryMapping> mappings) {
        this.mappings = mappings;
    }
}
